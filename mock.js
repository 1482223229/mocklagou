const reGroup = require('./json/parent_reGroup.json');
const listdata = require('./json/list_data.json');

function Mock(app) {
  app.get('/list/data', function(req, res) {
    res.json(listdata);
  });
  app.get('/regroup', function(req, res) {
    res.json(reGroup);
  });
}

module.exports = Mock;